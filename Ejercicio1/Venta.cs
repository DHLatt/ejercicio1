﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ejercicio1
{
    public class Venta
    {
        private int id;
        public List<Producto> Productos=new List<Producto>();

        public Venta(int id)
        {
            this.id=id;
        }

        public void AgregarProducto(Producto producto)
        {
            Productos.Add(producto);
        }

        public void EliminarProducto(Producto producto)
        {
            Productos.Remove(producto);
        }

        public override string ToString()
        {
            return  "Venta " + this.id;
        }
    }
}