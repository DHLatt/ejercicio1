﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ejercicio1
{
    public class Producto
    {
        public string codigoDeBarras;
        public string nombre;
        public string descripcion;
        public float precio;

        public Producto(string codigoDeBarras, string nombre, string descripcion, float precio)
        {
            this.codigoDeBarras=codigoDeBarras;
            this.nombre=nombre;
            this.descripcion=descripcion;
            this.precio=precio;
        }

        public override string ToString()
        {
            return nombre + ": " + precio;
        }
    }
}